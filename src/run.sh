#!/bin/bash
# para correr:
# ./run nombre_ejecutable
# donde nombre_ejecutable es cell, 1D_FD, 1D_CN

if [ $# -lt 1 ]; then
echo necesito el nombre del ejecutable para operar
fi

if [ -a ../bin/$1 ]; then
echo ejecutando ../bin/$1
mkdir -p ../images/$1
./../bin/$1 > ../images/$1/out
echo OK! archivo de salida en ../images/$1/out
else
echo ingrese el nombre valido
fi
