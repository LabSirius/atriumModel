#!/usr/bin/bash
# this scrip is only for test simulationCell.cc file
# it run for x minutes and each minute save data plot
# it is usefull for testing state of cell view current and voltaje
# this script use printimages.py script

mkdir -p ../images/cell/
for i in {0..10}
do
    a=`expr 60 \* $i`;
    echo "running for $i minutes with $a segundos"
    ../bin/cell $a  > ../images/cell/out$i
    mkdir -p ../images/cell/minuto$i
    echo "ploting data to ../images/cell/minuto$i"
    ./printImages.py ../images/cell/out$i minuto$i
done
